function emptyOrRows(rows) {
  if (!rows) {
    return [];
  }

  return rows;
}

function emptyOrFirst(rows) {
  if (!rows) {
    return {};
  }
  
  return rows[0];
}

module.exports = {
  emptyOrRows,
  emptyOrFirst
}