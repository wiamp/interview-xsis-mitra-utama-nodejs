const express = require('express');
const router = express.Router();
const movie = require('../services/movie');

// GET ALL
router.get('/', async function (req, res, next) {
  try {
    res.json(await movie.getAll());
  } catch (err) {
    console.error(`Error while getting movies `, err.message);
    next(err);
  }
});

// GET BY ID
router.get('/:id', async function (req, res, next) {
  try {
    res.json(await movie.getById(req.params.id));
  } catch (err) {
    console.error(`Error while getting movies `, err.message);
    next(err);
  }
});

// POST
router.post('/', async function (req, res, next) {
  try {
    res.json(await movie.create(req.body));
  } catch (err) {
    console.error(`Error while creating movie`, err.message);
    next(err);
  }
});

// PUT
router.put('/:id', async function (req, res, next) {
  try {
    res.json(await movie.update(req.params.id, req.body));
  } catch (err) {
    console.error(`Error while creating movie`, err.message);
    next(err);
  }
});

// DELETE
router.delete('/:id', async function(req, res, next) {
  try {
    res.json(await movie.softDelete(req.params.id));
  } catch (err) {
    console.error(`Error while deleting movie`, err.message);
    next(err);
  }
});

module.exports = router;