const express = require('express');

const app = express();

const movieRouter = require("./routes/movie");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/", (req, res) => { res.json({ message: "ok" }) });
app.use("/movie", movieRouter);

// Middleware error handling
app.use((err, req, res, next) => {
  const statusCode = err.statusCode || 500;
  console.error(err.message, err.stack);
  res.status(statusCode).json({ message: err.message });
  return;
});

app.listen(3000);