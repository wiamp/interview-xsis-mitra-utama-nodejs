const db = require('./db');
const helper = require('../helper');

async function getAll() {
  const rows = await db.query(`
    SELECT 
      id,
      title,
      description,
      rating,
      image,
      created_at,
      updated_at
    FROM movie m
    WHERE m.deleted_at IS NULL
  `);

  const data = helper.emptyOrRows(rows);

  return {
    data
  }
}

async function getById(id) {
  const rows = await db.query(`
    SELECT 
      id,
      title,
      description,
      rating,
      image,
      created_at,
      updated_at
    FROM movie m
    WHERE m.deleted_at IS NULL
      AND m.id = ?
    LIMIT 1
  `, [
    id
  ]);

  const data = helper.emptyOrFirst(rows);

  return {
    data
  }
}

async function create(data) {
  const result = await db.query(`
    INSERT INTO movie (
      title,
      description,
      rating,
      image
    ) VALUES (?, ?, ?, ?)
  `, [
    data.title,
    data.description,
    data.rating,
    data.image
  ]);

  let message = 'Error in creating movie data';

  if (result.affectedRows) {
    message = 'Movie data created successfully';
  }

  return { message };
}

async function update(id, data) {
  const result = await db.query(`
    UPDATE movie
    SET title = ?,
        description = ?,
        rating = ?,
        image = ?
    WHERE id = ?
  `, [
    data.title,
    data.description,
    data.rating,
    data.image,
    id
  ]);

  let message = 'Error in updating movie data';

  if (result.affectedRows) {
    message = 'Movie data updated successfully';
  }

  return { message };
}


async function softDelete(id) {
  const result = await db.query(`
    UPDATE movie
    SET deleted_at = NOW()
    WHERE id = ?
  `, [id]);

  let message = 'Error in deleting movie data';

  if (result.affectedRows) {
    message = 'Movie data deleted successfully';
  }

  return { message };
}

module.exports = {
  getAll,
  getById,
  create,
  update,
  softDelete
}