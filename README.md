## Specification

This project developed using NodeJS with ExpressJS
Node version: v16.20.0
NPM version: 8.19.4

## Installation

- Create database and set database configuration in config.js
- Import database.sql into database
- Config your database configuration in config.js
- Make sure you already install nodejs in your machine
- Copy files into your development folder and open terminal in root folder
- Type in terminal `npm i` to install all requirement packages
- Run the code by typing `node index`